CREATE TABLE "Ersatzteil" (
  "ID" int  NOT NULL,
  "MaschinenID" Int NOT NULL,
  "Bezeichnung" varchar(32)
);

CREATE TABLE "Kunde" (
  "KdNr" int NOT NULL,
  "Name" varchar(32) NOT NULL
);

CREATE TABLE "Kunde_Maschine" (
  "KdNr" int NOT NULL,
  "MaschinenID" int NOT NULL,
  "Date" date NOT NULL,
  "Produzierte_Einheiten" int
);

CREATE TABLE "Maschine" (
  "MaschinenID" int NOT NULL,
  "Bezeichnung" varchar(32) NOT NULL
);

CREATE TABLE "Techniker" (
  "ID_Nr" int NOT NULL,
  "MaschinenID" int NOT NULL,
  "Nachname" varchar(32) NOT NULL
);

ALTER TABLE "Ersatzteil"
  ADD PRIMARY KEY ("ID");

ALTER TABLE "Kunde"
  ADD PRIMARY KEY ("KdNr");

ALTER TABLE "Kunde_Maschine"
  ADD PRIMARY KEY ("KdNr","MaschinenID","Date");

ALTER TABLE "Maschine"
  ADD PRIMARY KEY ("MaschinenID");

ALTER TABLE "Techniker"
  ADD PRIMARY KEY ("ID_Nr");

ALTER TABLE "Ersatzteil"
	ADD FOREIGN KEY (MaschinenID) REFERENCES "Maschine" ("MaschinenID");

ALTER TABLE "Kunde_Maschine"
 ADD FOREIGN KEY ("MaschinenID") REFERENCES "Maschine" ("MaschinenID"),
  FOREIGN KEY ("KdNr") REFERENCES "Kunde" ("KdNr");

ALTER TABLE "Techniker"
 ADD FOREIGN KEY ("MaschinenID") REFERENCES "Maschine" ("MaschinenID");


INSERT INTO Kunde
           (KdNr
           ,Name)
     VALUES
           (1,'Schmidt'),
		   (2,'M�ller');


INSERT INTO Maschine
           (MaschinenID
           ,Bezeichnung)
     VALUES
           (1, 'Druckmaschine'),
		   (2, 'Pressmuttermaschinie');


INSERT INTO [dbo].[Techniker]
           ([ID_Nr]
           ,[MaschinenID]
           ,[Nachname])
     VALUES
           (1,1, 'Honul'),
		   (2,2, 'Ketsara');

INSERT INTO [dbo].[Ersatzteil]
           ([ID]
           ,[MaschinenID]
           ,[Bezeichnung])
     VALUES
           (1, 1, 'Schraube'),
		   (2, 2, 'Mutter');
