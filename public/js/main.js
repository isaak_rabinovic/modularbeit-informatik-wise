// Decode Unicode codePoint

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("eingabe").addEventListener("click", auslesemethode);
});

function auslesemethode (){
    var codePoint = document.getElementById("codepunkt").value;    // zugriff Auf HTML-Element: Eingabe String
    codePoint = codePoint.toUpperCase().replace('\\U+','');
    document.getElementById("decoded").innerHTML = hex2bin(codePoint);
    console.log(codePoint);
 }
 
function hex2bin(hexWert){
    return parseInt(hexWert, 16).toString(2).padStart(8, '0');
}

 // \U+0049\U+0053\U+0041\U+0041\U+004B
 // ISAA = 01001001 01010011 01000001 01000001 